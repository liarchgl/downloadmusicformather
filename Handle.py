import requests
import bs4
import codecs
import re
import urllib

urlSearch = "https://api.imjad.cn/cloudmusic/?type=search&search_type=1&s="
ids = [295175, 151467, 229010, 25642213, 518896364, 135778, 222046, 103643, 257318, 237661, 300474, 5230924, 5270877, 261727, 400581071, 5270992, 5280045, 278686, 170749, 27552550, 289145, 329125, 125264, 25721168, 226187, 226156, 229214, 317147, 257379, 5263324, 330614, 317326, 5274757, 330669, 235985, 316940, 242535, 276341, 27566967, 28718603, 29748139, 410715816, 25981347, 26418847, 571338439, 169628, 5242587, 27876076, 162664, 103886, 5234405, 5242710, 5249499, 26026892, 196816, 418602948, 169708, 4876565, 37853492, 5230116, 25642639, 26599005, 27731369, 26599533, 5248396, 5251482, 477206035, 130924, 30284175, 5234390, 303279, 28748386, 223279, 5237368, 33418866, 5235273, 5248267, 33071177, 149791, 25986119, 101272, 212605, 130637, 151818, 5254813, 535629290, 571473745, 102238, 130538, 130843, 222729, 143403, 60394, 436699244, 122535, 5242589, 30284185, 5238410, 5234407, 151985, 477387667, 163123, 32703005, 130606, 435552706, 212580, 26347023, 392907, 27901044, 221847]
urlHead = "http://music.163.com/song/media/outer/url?id="

def readHtml():
    with codecs.open("wait.html", "r", "gbk") as html:
        soup = bs4.BeautifulSoup(html.read())
        table = soup.find_all(class_="m-table")
        print(len(table))

def getIds():
    with codecs.open("wait.html", "r", "utf-8") as html:
        text = html.read()

        pattern = re.compile("\"/song\?id=[0-9]+\"")
        mat = re.findall(pattern, text)
        with codecs.open("res.txt", "w", "utf-8") as res:
            for ii in mat:
                res.write(ii[10:-1]+"\n")

def download():
    for i in ids:
        url = urlHead+str(i)
        con = requests.get(url)
        print(con.text)
        with(open("Music/"+str(i)+".mp3", "wb")) as mp3:
            mp3.write(con.content)
            mp3.close()

def id2url():
    with(codecs.open("urls.txt", "w", "utf-8")) as urlF:
        for id in ids :
            urlF.write(urlHead + str(id) + "\n")

if __name__ == "__main__":
    id2url()